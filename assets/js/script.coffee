angular.module 'app', [
  'sc.app.helpers'
]

.controller 'MainCtrl', [
  '$scope', 'scToggle', '$document', '$timeout'
  ($scope, toggle, $document, $timeout)->
    $scope.cliente = {}

    $scope.lista = [
      { id: 96,  title: "Administrar Condomínios" }
      { id: 53,  title: "Adriático" }
      { id: 167, title: "Alegria Santa Luzia" }
      { id: 97,  title: "Âmbar" }
      { id: 52,  title: "Ambar Stlyle" }
      { id: 43,  title: "Ametista" }
      { id: 32,  title: "Amsterdam" }
      { id: 139, title: "A Parceria Imóveis" }
      { id: 45,  title: "Ariadne" }
      { id: 26,  title: "Athenas" }
      { id: 202, title: "Atmosphere Front Park" }
      { id: 143, title: "Borges Landeiro Plaza" }
      { id: 172, title: "Borges Landeiro Santorini" }
      { id: 142, title: "Boulevard" }
      { id: 63,  title: "Breeze" }
      { id: 217, title: "Brisas Do Madeira" }
      { id: 102, title: "Brise" }
      { id: 101, title: "Cachoeira Bom Sucesso" }
      { id: 158, title: "Caldas Novas Ville" }
      { id: 137, title: "Caliandra Club" }
      { id: 154, title: "Caminho do Mar" }
      { id: 226, title: "Campo Bello Residence" }
      { id: 192, title: "Castell Di Villa Rica" }
    ]

    $scope.selecione = (item)->
      $scope.cliente = item

    $scope.dropdown = new toggle
      onOpen: ->
        @activeIdx = 0

    $scope.keyHandler =
      init: ->
        @elemListId = ".box-dropdown"
        @elemList = angular.element(@elemListId).find("ul")
      elemItemSelected: ->
        @elemList.find(".pre-selected")
      scrollToItem: ->
        list = $scope.keyHandler.elemList
        item = $scope.keyHandler.elemItemSelected()
        list.scrollTo((list.scrollTop() - list.offset().top + item.offset().top) - 10)
      exec: (event)->
        keyCode = event.which || event.keyCode

        if $scope.dropdown.closed && keyCode not in [keyMap.ESC, keyMap.BACKSPACE, keyMap.ENTER, keyMap.TAB]
          $scope.dropdown.open()
          return

        if $scope.dropdown.opened
          _lista = $scope.lista
          _len = _lista.length
          _idx = $scope.dropdown.activeIdx

          switch keyCode
            when keyMap.TAB
              $scope.dropdown.close()
            when keyMap.ARROW.UP
              $scope.dropdown.activeIdx = (if _idx then _idx else _len) - 1
              $timeout @scrollToItem, 80
            when keyMap.ARROW.DOWN
              $scope.dropdown.activeIdx = (_idx+1) % _len
              $timeout @scrollToItem, 80
            when keyMap.ESC
              $scope.dropdown.close()
              if _len
                event.preventDefault()
                event.stopPropagation()
            when keyMap.ENTER
              $scope.selecione _lista[_idx]
              event.preventDefault()
              event.stopPropagation()

    $scope.keyHandler.init()

    angular.element($scope.keyHandler.elemListId).on "focus click", () ->
      angular.element($scope.keyHandler.elemListId).find("input").focus()

    fecharDropdown = (evt)->
      $scope.$apply ->
        target = evt.target
        $target = $(target)
        dropdown = $target.closest($scope.keyHandler.elemListId)
        $scope.dropdown.close() if dropdown.length == 0
        return

    $document.bind 'click', fecharDropdown

    $scope.$on '$destroy', ->
      $document.unbind 'click', fecharDropdown
]
